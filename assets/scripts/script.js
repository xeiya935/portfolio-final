let profileBtn = document.querySelector('#profile-btn')
let profileSection = document.querySelector('#profile-section')
let projectsBtn = document.querySelector('#projects-btn')
let projectsSection = document.querySelector('#projects-section')
let skillsBtn = document.querySelector('#skills-btn')
let skillsSection = document.querySelector('#skills-section')
let educationBtn = document.querySelector('#education-btn')
let educationSection = document.querySelector('#education-section')
let experienceBtn = document.querySelector('#experience-btn')
let experienceSection = document.querySelector('#experience-section')
let currentPage = "profile";

profileBtn.addEventListener('click', function(){
	profileSection.style.display = "block";
	projectsSection.style.display = "none";
	skillsSection.style.display = "none";
	educationSection.style.display = "none";
	experienceSection.style.display = "none";
})

projectsBtn.addEventListener('click', function(){
	projectsSection.style.display = "block";
	profileSection.style.display = "none";
	skillsSection.style.display = "none";
	educationSection.style.display = "none";
	experienceSection.style.display = "none";
})

skillsBtn.addEventListener('click', function(){
	skillsSection.style.display = "block";
	profileSection.style.display = "none";
	projectsSection.style.display = "none";
	educationSection.style.display = "none";
	experienceSection.style.display = "none";
})

educationBtn.addEventListener('click', function(){
	educationSection.style.display = "block";
	profileSection.style.display = "none";
	projectsSection.style.display = "none";
	skillsSection.style.display = "none";
	experienceSection.style.display = "none";
})

experienceBtn.addEventListener('click', function(){
	experienceSection.style.display = "block";
	profileSection.style.display = "none";
	projectsSection.style.display = "none";
	skillsSection.style.display = "none";
	educationSection.style.display = "none";
})